package com.gabi.shoutboxx

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.gabiShoutbox.R


class SettingsActivity : AppCompatActivity() {

    lateinit var loginInput: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        loginInput = findViewById(R.id.loginInput);
        val button = findViewById<Button>(R.id.loginButton)
        loginInput.setText(loadData())

        loadLogin()

        button.setOnClickListener {
            saveData()
            Log.d("GG", " KLINALAS SAVE LOGIN")
            val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
            val defaultValue = "default_login"
            Log.d("login to:",sharedPref.getString("saved_login", defaultValue.toString()))

        }
    }

    private fun loadData(): String {
        val sharedPreferences = getSharedPreferences("shared preferences", Context.MODE_PRIVATE)
        return sharedPreferences.getString("login", "")!!
    }

    fun saveData() {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return
        val editor = sharedPref.edit()
        editor.putString("saved_login", loginInput.text.toString())
        editor.commit()
        editor.apply()
        finish()
    }

    private fun loadLogin() {
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = "default_login"
        loginInput.setText(sharedPref.getString("saved_login", defaultValue))
    }
}
