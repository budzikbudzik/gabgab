package com.example.gabiShoutbox

import JsonPlaceholderAPI
import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_shoutbox.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class ShoutboxFragment : Fragment(), MessageAdapter.OnItemClickListener {

    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var messagesData: Array<GabMessage>
    private lateinit var login: String
    private lateinit var jsonPlaceholderAPI: JsonPlaceholderAPI
    private lateinit var retrofit: Retrofit
    val thread = Executors.newSingleThreadScheduledExecutor()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        loadLogin()

        val root = inflater.inflate(R.layout.fragment_shoutbox, container, false)

        ////API json
        val baseUrl = "http://tgryl.pl/"
        retrofit = Retrofit.Builder().baseUrl(baseUrl)
            .addConverterFactory(
                GsonConverterFactory
                    .create()
            )
            .build()
        jsonPlaceholderAPI = retrofit.create(JsonPlaceholderAPI::class.java)
        ////API json

        beginRefreshing()

        swipeRefresh = root.findViewById(R.id.swipeRefresh)
        swipeRefresh.setOnRefreshListener {
            if (checkNetworkConnection()) {
                createJsonGet(jsonPlaceholderAPI)
                swipeRefresh.isRefreshing = false
                Log.d("internet:", "Messages refreshed")
                Toast.makeText(context, "Internet: Messages refreshed", Toast.LENGTH_SHORT).show()

            } else {
                Log.d("internet:", "no internet")
                Toast.makeText(context, "Internet: no connection", Toast.LENGTH_SHORT).show()
            }
        }
        return root
    }

    fun updateData() {
        if (recyclerView != null) {
            messagesData.reverse();
            recyclerView.adapter = MessageAdapter(messagesData, this)
            recyclerView.layoutManager = LinearLayoutManager(context)
            recyclerView.setHasFixedSize(true)
        }
    }

    fun createJsonGet(jsonPlaceholderAPI: JsonPlaceholderAPI) {
        val call = jsonPlaceholderAPI.getMessageArray()
        call!!.enqueue(object : Callback<Array<GabMessage>?> {
            override fun onResponse(
                call: Call<Array<GabMessage>?>,
                response: Response<Array<GabMessage>?>
            ) {
                if (!response.isSuccessful) {
                    println("Code: " + response.code())
                    return
                }
                messagesData = response.body()!!
                updateData()
            }

            override fun onFailure(
                call: Call<Array<GabMessage>?>,
                t: Throwable
            ) {
                println(t.message)
            }
        })
    }

    fun checkNetworkConnection(): Boolean {
        val connectivityManager =
            context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

    override fun onItemClick(//dzialanie edycji - klikniecia na cokolwiek z listy wiadomosci
        item: GabMessage, position: Int
    ) {
        // login="test"
        Log.d("BLA", "KLINALES COS NA LISCIE")
        Log.d("LOGIN TO:::::::::::::::::::::::::::::::::::::::::", login.toString())
        if (login == item.login) {
            val bundle = Bundle()
            bundle.putString("login", item.login)
            bundle.putString("id", item.id)
            bundle.putString("date_hour", item.date)
            bundle.putString("content", item.content)
            val fragment: Fragment = EditFragment()
            fragment.arguments = bundle
            val fragmentManager: FragmentManager? = fragmentManager
            fragmentManager?.beginTransaction()
                ?.replace(R.id.nav_host_fragment, fragment)
                ?.commit()
        } else {
            Toast.makeText(context, "Możesz edytować tylko swoje wiadomosci", Toast.LENGTH_SHORT)
                .show()
        }
    }


    private fun loadLogin() {
        val sharedPref = activity!!.getPreferences(Context.MODE_PRIVATE) ?: return
        val defaultValue = "default_login"
        login = sharedPref.getString("saved_login", defaultValue).toString()
    }

    fun beginRefreshing() {//TODO::??? NIEDZIALA
        thread.scheduleAtFixedRate({
            if (checkNetworkConnection()) {
                createJsonGet(jsonPlaceholderAPI)
                Log.d("Executors thread: ", "Messages refreshed automatically ")
            } else {
                Log.d(
                    "Executors thread: ",
                    "Cant automatically refresh messages - no internet connection!"
                )
            }
        }, 0, 5, TimeUnit.SECONDS)
    }
}


