
import com.example.gabiShoutbox.GabMessage
import retrofit2.Call
import retrofit2.http.*

interface JsonPlaceholderAPI {
    @GET("shoutbox/messages")
    fun getMessageArray(): Call<Array<GabMessage>?>?

    @POST("shoutbox/message")
    fun createPost(@Body GabMessage: GabMessage): Call<GabMessage>

    @PUT("shoutbox/message/{id}")
    fun createPut(
        @Path("id") id: String,
        @Body exampleItem: GabMessage
    ): Call<GabMessage>
}